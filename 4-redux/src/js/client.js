import { createStore } from "redux";

const reducer = function(state, action) {
    if (action.type === "INC") {
        return state + action.payload;
    }
    if (action.type === "DEC") {
        return state - action.payload;
    }
    if (action.type === "OBJECT") {
        return action.newguy;
    }
    return state;
}

const store = createStore(reducer, 0);

store.subscribe(() => {
    console.log("store changed", store.getState())
})

store.dispatch({type: "INC", payload: 1})
store.dispatch({type: "INC", payload: 4})
store.dispatch({type: "INC", payload: 55})
store.dispatch({type: "INC", payload: 888})
store.dispatch({type: "DEC", payload: 777})
store.dispatch({type: "OBJECT", newguy: {id: 1, name: "Tomas"}})